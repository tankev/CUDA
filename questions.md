# Unresolved Questions

This is a list of questions that came up during my experiments that I'll eventually revisit and answer but just haven't gotten around to yet.

- What happens when you access invalid memory (e.g. memory that does not belong to you) on the GPU? What sorts of memory protections does the GPU enforce and how do they compare to the memory protections that system memory enforces?
- What is the proper way to handle errors that occur when calling CUDA API functions and is there some sort of library that does basic error checking and prints human-readable messages?
- How do you debug a CUDA program using the `cuda-gdb` tool?
- Why does the hardware impose a hard limit on the number of blocks you can spawn for a kernel invocation? And why is it 65535? Will this ever change in the future and do you ever really run into issues with this ceiling?
- What would it take to add intellisense to the CUDA language extension for VS Code? This would really aid the development for CUDA programs, and I think it would open up GPU programming to an even wider swath of society. Maybe VSCode is not what CUDA programmers use? If so, what _do_ they use?
- Is there some sort of tool that I can use to monitor the GPU? For the CPU, it is easy to see what processes it is running, how much CPU time those processes have taken up, and various other things like how much system memory is being used. Is there some sort of tool to do this for the GPU?
- I read that thread management and scheduling is done in _hardware_ on the GPU? How is this possible?
- What exactly are the costs to spin up and tear down a thread on the GPU? This is nontrivial for a CPU, which is why we have thread pools, so what are the associated costs for doing this on a GPU? It must be extremely low, right?
- Are there other C++ formatters other than clang-format that are more... pythonic? In particular, I really dislike how function calls with lots of arguments is split up between multiple lines. Things are no longer aligned in neat little tabs. It would be great to get a pythonic formatter.

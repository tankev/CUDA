#include <stdio.h>

/**
 * This program will print information about your CUDA devices.
 *
 * For the definitive reference on what properties there are, click here:
 * https://docs.nvidia.com/cuda/cuda-runtime-api/structcudaDeviceProp.html.
 */
int main(void) {
  // Determine the number of CUDA devices.
  int count;
  cudaGetDeviceCount(&count);
  // Print the information about each CUDA device.
  for (int i = 0; i < count; i++) {
    cudaDeviceProp prop;
    cudaGetDeviceProperties(&prop, i);
    printf("---CUDA Device %d---\n", i);
    printf("Name: %s\n", prop.name);
    printf("Max Threads/Block: %d\n", prop.maxThreadsPerBlock);
    printf("Shared Memory/Block: %ldB\n", prop.sharedMemPerBlock);
  }
  return 0;
}
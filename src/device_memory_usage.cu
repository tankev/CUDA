#include <stdio.h>

#define BILLION 1000000000

const size_t N = 1;

size_t get_free_memory() {
  size_t free, total;
  cudaMemGetInfo(&free, &total);
  return free;
}

int main() {
  size_t free, total;
  cudaMemGetInfo(&free, &total);
  printf("Total memory:\t%.3fGB\n", (float)total / BILLION);
  printf("Free memory:\t%.3fGB\n", (float)free / BILLION);
  void *buffer;
  printf("\t[Event] Allocate %ldGB of memory\n", N);
  cudaError_t status = cudaMalloc(&buffer, N * BILLION);
  if (status == cudaSuccess) {
    printf("Free memory:\t%.3fGB\n", (float)get_free_memory() / BILLION);
    printf("\t[Event] Free %ldGB of memory\n", N);
    cudaFree(buffer);
    printf("Free memory:\t%.3fGB\n", (float)get_free_memory() / BILLION);
  } else if (status == cudaErrorInvalidValue)
    printf("❌ ERROR: invalid value.\n");
  else
    printf("❌ ERROR: launch failure.\n");
}
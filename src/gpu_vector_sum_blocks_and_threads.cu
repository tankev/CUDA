#include <stdio.h>

/* The size of our vector. */
#define N 10000

/**
 * This is a kernel, a function that runs on the GPU.
 */
__global__ void add(int *a, int *b, int *c) {
  int tid = blockIdx.x * blockDim.x + threadIdx.x;
  while (tid < N) {
    c[tid] = a[tid] + b[tid];
    // Increment by the number of threads launched.
    tid += gridDim.x * blockDim.x;
  }
}

int main(void) {
  // These are pointers to arrays in system memory.
  int a[N], b[N], c[N];
  // These are pointers to arrays in GPU memory.
  int *dev_a, *dev_b, *dev_c;
  // Allocate memory on the GPU.
  cudaMalloc(&dev_a, N * sizeof(int));
  cudaMalloc(&dev_b, N * sizeof(int));
  cudaMalloc(&dev_c, N * sizeof(int));
  // Populate the CPU summand arrays.
  for (int i = 0; i < N; i++) {
    a[i] = -i;
    b[i] = i * i;
  }
  // Copy the CPU summand arrays into the GPU summand arrays.
  cudaMemcpy(dev_a, a, N * sizeof(int), cudaMemcpyHostToDevice);
  cudaMemcpy(dev_b, b, N * sizeof(int), cudaMemcpyHostToDevice);
  // Add the GPU summand arrays.
  add<<<128, 128>>>(dev_a, dev_b, dev_c);
  // Copy the GPU output array into the CPU output array.
  cudaMemcpy(c, dev_c, N * sizeof(int), cudaMemcpyDeviceToHost);
  // Print the output array to the console.
  for (int i = 0; i < N; i++)
    printf("%d + %d = %d\n", a[i], b[i], c[i]);
  // Free memory on the GPU.
  cudaFree(dev_a);
  cudaFree(dev_b);
  cudaFree(dev_c);
  return 0;
}

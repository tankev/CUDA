/**
 * The CUDA API supports a bunch of built-in vector types of dimensions from
 * 1 to 4. Read all about them at the link below.
 * https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#built-in-vector-types
 *
 * There are a total of 12 different families of vectors.
 *
 *      [u]char[1234]
 *      [u]short[1234]
 *      [u]int[1234]
 *      [u]long[1234]
 *      [u]longlong[1234]
 *      float[1234]
 *      double[1234]
 *
 * They are specified above in the form of regular expressions.
 */
#include <iostream>
using namespace std;

void print_int1(int1 int_vector) {
  cout << "<int1[" << int_vector.x << "]>" << endl;
}

void print_int2(int2 int_vector) {
  cout << "<int2[" << int_vector.x << ", " << int_vector.y << "]>" << endl;
}

void print_int3(int3 int_vector) {
  cout << "<int3[" << int_vector.x << ", " << int_vector.y << ", "
       << int_vector.z << "]>" << endl;
}

void print_int4(int4 int_vector) {
  cout << "<int4[" << int_vector.x << ", " << int_vector.y << ", "
       << int_vector.z << ", " << int_vector.w << "]>" << endl;
}

void print_dim3(dim3 dimensions) {
  cout << "<dim3[" << dimensions.x << ", " << dimensions.y << ", "
       << dimensions.z << "]>" << endl;
}

int main() {
  int1 vector_1 = make_int1(3);
  print_int1(vector_1);
  int2 vector_2 = make_int2(1, 4);
  print_int2(vector_2);
  int3 vector_3 = make_int3(1, 5, 9);
  print_int3(vector_3);
  int4 vector_4 = make_int4(2, 6, 5, 3);
  print_int4(vector_4);
  // The dim3 vector type is derived from uint3 and is specialized for
  // describing dimensions of grids and blocks of GPU threads.
  dim3 dimensions1(31);
  print_dim3(dimensions1);
  dim3 dimensions2(41, 59);
  print_dim3(dimensions2);
  dim3 dimensions3(26, 53, 58);
  print_dim3(dimensions3);
}
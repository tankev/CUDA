#include <stdio.h>

/**
 * This program prints the number of available CUDA GPUs on your machine.
 */
int main(void) {
  int count;
  cudaGetDeviceCount(&count);
  printf("Number of CUDA GPUs on your machine: %d\n", count);
  return 0;
}
![](https://img.shields.io/github/license/kevtan/CUDA)
![](https://img.shields.io/github/repo-size/kevtan/CUDA)
![](https://img.shields.io/badge/nvcc-10.1-orange)
![](https://img.shields.io/github/languages/top/kevtan/CUDA?color=purple)
![](https://img.shields.io/badge/gitmoji-%20😜%20😍-FFDD67.svg)

# CUDA

A sandbox for me to learn the basics of the CUDA API.

## 5D Programming Model

There are 3 thought primitives in the CUDA programming model: the grid, the block, and the thread. A grid is a 2D array of blocks. A block is a 3D array of threads. A thread is, well, something that is capable of performing computational work. A kernel can probe its surroundings and understand the 5D structure in which it resides through the `gridDim` and `blockDim` variables that the CUDA runtime system provides. These two variables are actually what you specify inside the triple angle brackets <<< and >>> in a kernel call.

- `gridDim`: a `dim3` object whose `x` and `y` attributes are used.
- `blockDim`: a `dim3` object whose `x`, `y`, and `z` attributes are used.

After the kernel knows the surrounding context, it can then use the built-in `blockIdx` and `threadIdx` variables (both `uint3` objects) to determine which particular location in the 5D structure it is in. Using all of this information, it can then make a decision as to what part of the data it is in charge of computing so as to not step on another kernel's toes. Note that CUDA uses zero-indexing and not one-indexing!

To build a little more intuition with `blockIdx` and `threadIdx` mean, I like to remember what each are indexes _into_. In particular,`blockIdx` is the index of the executing block _within_ the executing grid and `threadIdx` is the index of the executing thread _within_ the executing block.

In the following code snippet, you can see what a CUDA kernel call looks like. As discussed previously, both `n` and `m` are `dim3` objects that represent the `gridDim` and `blockDim` variables, respectively.

![A kernel call.](./assets/kernel_call.png)

## Magic Attributes

In Python, we have magic methods and variables like `__str__` and `__name__`. In CUDA C, we have "magic attributes" that look the same but are functionally different. In particular, they are essentially GNU attributes that are used to inform the CUDA compiler `nvcc` of various information. Here are some examples:

- `__global__`: Prepend this to a function declaration to inform `nvcc` that this is a kernel and will be executed on the device.
- `__device__`: Prepend this to a function declaration to inform `nvcc` that this should only be run on the device. In particular, functions with this attribute can only be called from within other `__device__` functions or within a `__global__` function.
- `__shared__`: Prepend this to a variable declaration inside of a `__global__` or `__device__` function to make it reside in shared memory. Shared memory is memory that is shared by the threads in a block (each block gets its own copy) but completely insulated from threads outside the block.

## Thread Synchronization

As with any kind of parallel computing, there needs to be sort of synchronization system for your threads to communicate and rendezvous with each other. The CUDA API exports a variety of synchronization primitives. For instance, there is a `__syncthreads()` method that will synchronize all the threads within a particular block. The guarantee it gives is that if any thread is executing code after `__syncthreads()`, every other thread has finished executing code at least up to `__syncthreads()`.

## Debugging

The CUDA debugger `cuda-gdb` is regular `gdb` with a few extensions to make it easier to debug CUDA programs. In particular, there's a new "cuda" family of commands that you can execute in the debugger. You can see what these commands do by running `cuda-gdb` and typing "help cuda" at the debugger prompt; see the following image.

![A listing of the CUDA-related commands that cuda-gdb supports.](./assets/cuda-gdb_cuda_commands.png)
